/** Type information.
 * @copyright 2022-2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

/** An object representing a space on the perimeter of a Trax position.
 * x,y: the coordinates of the space
 * c: the color present beside the space (w, b, l: both w and b, r: none)
 * t: the type of possible moves: n-normal, x-none, c-cave, C-restricted cave
 * b,w: if present, the number of the line to match with its other end
 * pb, pw: if present, can pair with the next line in one turn
 * xb, xw: the label of this space for each color
 */
type EdgeLocation = {
  x: number
  y: number
  c: string
  t: string
  b?: number
  w?: number
  pb?: boolean
  pw?: boolean
  zb?: string
  zw?: string
  idx?: number
}

type EdgeObject = {
  b: string
  w: string
  edge: RawEdge
}

/** Object representing a concrete threat found in a position. */
type FoundThreat = {
  threat: string
  match: string
  at: number
  value: number
  level: number
}

/** Threats found for each color that cannot actually be activated. */
type FaultyThreats = {
  b: FoundThreat[]
  w: FoundThreat[]
}

/** An object with keys representing each level, with arrays for each threat of
 * that level. */
type ColorThreats = Record<string, FoundThreat[]>

/** All the threats found for both the white and black player. */
type FoundThreatsCollection = { b: ColorThreats; w: ColorThreats }

/** Scores for each player. */
type Scores = {
  b: number
  w: number
}

/** An object used to track an analysis on a position. */
type PositionScore = {
  move: string
  score: number
  analysis?: Analysis
}

/** Array of objects representing the spaces surrounding the perimeter of a
 * Trax position. */
type RawEdge = EdgeLocation[]

/** Threat definition. */
type Threat = {
  depth: number
  pattern: string
  rx: RegExp
  value: number
}
