/** Threats database: hand-coded threats
 * @copyright 2022-2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

const REGEX = {
  // Some of these are one for one:
  // a: White that pairs with the next white
  A: '[am]', // White maybe in a hollow that pairs with next white
  // c: White that forms a connectable pair with next white
  C: '[cp]', // White maybe in a hollow connectable pair with next white
  l: '[lmp]', // Any White in a hollow
  // m: "a" in a hollow
  // p: "c" in a hollow)
  w: '[acw]', // Any white except in hollows
  W: '[aclmpw]', // Any white at all
  '!': '(?:b[acw])*?', // Ripple black/white
  '¡': '(?:[acw]b)*?', // Ripple white/black
  '*': 'b*?', // Zero or more black pieces
  '?': 'b?', // Zero or one black piece
}

export class Threat {
  #depth = 0
  #pattern = ''
  #rx
  #reforms = false

  constructor(depth, pattern, reforms = false) {
    this.#depth = depth
    this.#pattern = pattern
    this.#reforms = reforms

    let rx = ''
    for (const char of pattern) {
      rx += REGEX[char] || char
    }

    // Create the regex with lookahead assertions to allow overlapping matches
    this.#rx = new RegExp('(?=(' + rx + '))', 'g')
  }

  get depth() {
    return this.#depth
  }

  get pattern() {
    return this.#pattern
  }

  get rx() {
    return this.#rx
  }

  get value() {
    return this.#reforms ? 2 : 1
  }

  // To make life at the REPL a little easier
  [Symbol.for('nodejs.util.inspect.custom')](_, out) {
    const style = out.stylize
    const display = [
      '[' + style('Threat', 'special') + ']',
      '{',
      style(this.#pattern, 'string') + ':',
      style(this.#depth, 'number'),
      this.#reforms ? '!' : '',
      '}',
    ]
    return display.filter((d) => d !== '').join(' ')
  }
}

export class ThreatsDB {
  #db = {}
  #depth = 0

  constructor(corners) {
    for (const corner of corners) {
      this.add(0, corner)
    }
  }

  compose(depth, pattern) {
    pattern = pattern.replace(/(?<=.)A/g, 'a').replace(/W(?=.)/g, 'w')
    return this.add(depth, pattern)
  }

  add(depth, pattern, reforms = false) {
    this.#depth = Math.max(depth, this.#depth)
    if (pattern in this.#db) throw new Error('Duplicate pattern: ' + pattern)
    this.#db[pattern] = new Threat(depth, pattern, reforms)
    return this.#db[pattern]
  }

  level(depth) {
    return Object.values(this.#db).filter((p) => p.depth === depth)
  }

  get list() {
    return Object.values(this.#db)
  }

  get maxDepth() {
    return this.#depth
  }

  // To make life at the REPL a little easier
  [Symbol.for('nodejs.util.inspect.custom')](_, out) {
    const style = out.stylize
    const inspection = '[' + style('Threats', 'special') + ':'
    let total = 0
    const keys = ['{']

    for (let x = 0; x <= this.#depth; x++) {
      const count = this.level(x).length
      keys.push(style(x, 'string') + ':' + style(count, 'number'))
      total += count
    }

    return inspection + style(total, 'number') + '] ' + keys.join(' ') + ' }'
  }
}

const pre = true // This pattern can appear first in an L threat
const post = true // This pattern can come second in an L threat
const corners = {
  ArW: { pre, post }, // Simple corner
  AbrW: { post }, // Wide-first corner
  ArbW: { pre }, // Wide-second corner
  AbrbW: {}, // Wide corner
  'ab?bW': { pre, post }, // Flat corner and wide flat corner
  CW: { pre, post }, // Connectable pair
  CbW: {}, // Wide connectable pair
}

export const threats = new ThreatsDB(Object.keys(corners))

// Attacks
const attacks = [
  'a?W', // Simple attack and wide attack
  'm?W', // Simple and wide attack in a corner
]
attacks.map((a) => threats.add(1, a))

// Re-forming attacks
const reformAttacks = [
  'rm?lr', // Cave/wide cave attack
  'rbm?lbr', // Deep cave/deep wide cave attack (impossibly rare?)
  'ralmrW', // Attack at top of cave
  'arlmwr', // Reversed
]
reformAttacks.map((ra) => threats.add(1, ra, true))

const reformThreats = [
  'rp?lr', // Cave/wide cave threat
  'rbp?lbr', // Deep cave/deep wide cave threat (impossibly rare?)
  'rmbb???w', // Black corner with a long white loop potential
  'abb???lr', // Reverse
]
reformThreats.map((rt) => threats.add(2, rt))

// Create multi-stage threats
for (const one of Object.keys(corners)) {
  const start = one.replace('W', 'l')
  let end = one.replace(/[aA]/, 'm').replace('C', 'p')
  const pre = corners[one].post ? '?' : ''
  let post = corners[one].pre ? '?' : ''
  threats.compose(3, start + pre + '¡rm?W') // Attacking 3-stage threat
  threats.compose(3, 'a?lr!' + post + end) // Reverse
  threats.compose(3, one + '¡ab*bwr') // Corner by reforming hollow potential
  threats.compose(3, 'rab*bw!' + one) // Reverse
  threats.compose(3, one + '!brm*bW') // Corner by hollow attack, defends to edge
  threats.compose(3, one + '!bbrmW') // Same, but slightly different layout
  threats.compose(3, 'ab*lrb¡' + one) // Reverse
  threats.compose(3, 'alrbb¡' + one) // Reverse of different layout
  threats.compose(4, one + '!b*b¡ra?lr') // Partial edge with attack into hollow
  threats.compose(4, 'rm?wr!b*b¡' + one) // Reverse

  for (const two of Object.keys(corners)) {
    end = two.replace(/[aA]/, 'm').replace('C', 'p')
    post = corners[two].pre ? '?' : ''
    if (pre) {
      threats.compose(2, start + pre + '¡' + two) // Make an L
      threats.compose(3, start + pre + 'rb¡' + two) // Make a three-stage threat
      threats.compose(3, start + '?!rb¡' + two) // Attack to form an L threat
    }

    if (post) {
      threats.compose(2, one + post + '!' + end) // L
      threats.compose(3, one + post + '!br' + end) // Three-stage
      threats.compose(3, one + post + 'r' + end) // Defense forms an L
      threats.compose(3, one + '!br¡?' + end) // Attack to form an L threat
    }

    if (pre && post) {
      threats.compose(3, start + 'r' + end) // Defense forms an L
    }

    threats.compose(3, one + '!b*b¡' + two) // Edge threat
  }
}

threats.add(3, 'ralbmrW') // Attack into a wide cave
threats.add(3, 'Arlbmwr') // Reversed

threats.add(4, 'rb¡c?w!br') // Connectable pair on an edge
threats.add(4, 'ArlbmrW') // Wide cave ready for an attack

// Threats to solve puzzle iw1.trx
threats.add(5, 'A?rwarlrar?W') // Back to back white corners next to a corner
threats.add(4, 'A?rlr!bralrar?W') // ... activated ... becomes:
threats.add(4, 'A?rlrbrmrwar?W') // Corner surrounded by corners
threats.add(4, 'A?rlr!bramrar?W') // ... activated ...
threats.add(3, 'A?rlr!brm?wr!brmrW') // ... defended ...

// Threats to solve puzzle ib10.trx
threats.add(6, 'Arw!b?rarlr') // Sucker's Opening
threats.add(5, 'Arw!b?brbralr') // ... activated ... becomes:
threats.add(4, 'Arw!b?brb¡arbW') // Wide corner opposite nearly formed edge
threats.add(5, 'Arw!b?brb¡rmrW') // ... or hollow corner opposite same edge
threats.add(4, 'Arw!b?brb¡rmwr') // ... activated ...

// Threats to defend hw10.trx
threats.add(4, 'A?r?w!b?¡brb¡a?r?W') // Corner that completes an edge
