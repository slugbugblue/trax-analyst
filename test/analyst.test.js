/** Testing the analyst code
 * @copyright 2022-2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

import { inspect } from 'node:util'
import test from 'ava'

import { analyze, suggest } from '@slugbugblue/trax-analyst'
import { Trax } from '@slugbugblue/trax'

test('first edge', (t) => {
  const game = new Trax('trax', '@0/', 'ava')
  const { edge } = analyze(game)
  t.true(Array.isArray(edge.edge), 'has raw edge')
  t.is(typeof edge.b, 'string', 'has black string')
  t.is(typeof edge.w, 'string', 'has white string')
  t.deepEqual(
    edge.edge[0],
    { c: 'w', zw: 'a', zb: 'b', nw: '0', idx: 0, x: 0, y: -1, t: 'n', w: true },
    'raw edge contents',
  )
  t.is(edge.w, 'arbrbrar', 'white label')
  t.is(edge.b, 'brararbr', 'black label')
})

test('immutable analysis', (t) => {
  const game = new Trax('trax', '@0/', 'ava')
  const analysis = analyze(game)
  game.play('a0\\')
  t.is(game.move, 2, 'game is mutable')
  t.is(analysis.game.move, 1, 'analysis is immatable')
  analysis.game.play('a0\\')
  t.is(analysis.game.move, 1, 'even when we play directly on it')
  t.is(analysis.score, 0, 'equal game')
  const zero = Trax.point(0, 0)
  t.is(analysis.inCave(zero), false, 'tiles are not caves')
})

test('simple attacks', (t) => {
  const attack = new Trax('trax', '@0/ A0\\', 'ava')
  const { edge, threats, score } = analyze(attack)
  t.is(edge.w, 'brbbrbraar', 'white edge')
  t.is(edge.b, 'arwarwrbbr', 'black edge')
  t.is(threats.w?.[1]?.length, 1, 'white has an attack')
  t.is(threats.b?.[0]?.length, 2, 'black has two corners')
  t.is(score, 999_998, 'white has a huge advantage')
  const wideAttack = new Trax('trax', '@0+ A0\\ A3/', 'ava')
  const wide = analyze(wideAttack)
  t.is(wide.edge.w, 'brbbbrbrabar', 'white edge')
  t.is(wide.edge.b, 'arwwarwrbwbr', 'black edge')
  t.is(wide.threats.w?.[1]?.length, 1, 'white has an attack')
  t.is(wide.threats.b?.[0]?.length, 2, 'black has two corners')
  t.is(wide.score, -999_998, 'black desperately needs to defend')
})

test("sucker's opening", (t) => {
  const game = new Trax('trax', '@0/ @1/ B0\\')
  const { score } = analyze(game)
  t.is(score, 13, "don't be a sucker")
})

test('simple cave', (t) => {
  const game = new Trax('trax', '@0/ @1/ B0\\ @2+ @2/ A1+', 'ava')
  const { edge } = analyze(game)
  t.is(edge.w, 'wrpmrbrbbrbcwbrabr', 'white label')
  t.is(edge.b, 'brllrarwarwbbwrbar', 'black label')
})

test('cave with some invalid moves', (t) => {
  const game = new Trax('trax', '@0+ B1\\ @1\\ C2/ C3/ C4/ B4\\ A4+ @1/', 'ava')
  const { edge, threats } = analyze(game)
  t.is(edge.w, 'bbwarwwbarwabrwrbllawrbr', 'white label')
  t.is(edge.b, 'wwbbrbbwbrbbarbrwllbbrar', 'black label')
  // It would be useful if eventually we could detect that this was not a real attack
  t.is(threats.w?.[1]?.length, 1, 'oops -- FIX -- not really an attack')
})

test('dead cave', (t) => {
  const game = new Trax(
    'trax',
    '@0+ A0+ B1/ A3\\ A4+ B4\\ C4\\ C1+ D1\\ D4\\',
    'ava',
  )
  const { edge } = analyze(game)
  t.is(edge.w, 'wbbbrbrxxxxxxrbrwbabrwbbbr', 'white label')
  t.is(edge.b, 'bwwarwrxxxxxxrwrbwbarbwwar', 'black label')
})

test('re-forming cave threat', (t) => {
  const game = new Trax('trax', '@0/ @1/ B0\\ @2+ @2/ A1+ C3/', 'ava')
  const { edge, threats, score } = analyze(game)
  t.true(edge.w.includes('rmlr'), 'white has a re-forming attack')
  t.is(threats.w?.[0]?.length, 3, 'white has three corners')
  t.is(threats.w?.[1]?.length, 2, 're-forming white attack counts double')
  t.is(threats.w?.[2], undefined, 'white has no other two-stage threats')
  t.is(threats.w?.[3]?.length, 1, 'white also has an edge threat')
  t.is(score, -2_010_001, 'white has a huge advantage')
})

test('full 8x8 board', (t) => {
  const game = new Trax('trax8', '@0/ a0/ a0/ a0/ a0/ a0/ a0/ a0/', 'ava')
  const { edge } = analyze(game)
  t.is(edge.w, 'xrcbcbcbabrxrcbcbcbabr', 'cannot play on ends')
})

test('game over', (t) => {
  const game = new Trax('trax', '@0/ a0\\ @1/', 'ava')
  const analysis = analyze(game)
  t.is(analysis.score, Number.POSITIVE_INFINITY, 'infinite score')
})

test('faulty L', (t) => {
  const game = new Trax('trax', '@0/ B1\\ A2+ A3\\ B2\\ B3+ C3+ D3+', 'ava')
  const analysis = analyze(game)
  t.is(analysis.count.w(2), 1, 'found an L')
  analysis.validateThreats()
  t.is(analysis.count.w(2), 0, 'cannot activate a faulty L')
  t.is(analysis.faulty.w.length, 1, 'faulty threat')
})

test('faulty attack', (t) => {
  const game = new Trax(
    'trax',
    '@0/ A2+ A3+ A4\\ B3\\ B4+ B5\\ C5/ C4+ D4/ D5+ E5/ E4/',
    'ava',
  )
  const { options } = suggest(game)
  const moves = options.map((s) => s.move)
  t.false(moves.includes('E3/'), 'recommended E3/ for hb2.trx puzzle')
})

test('analysis inspection', (t) => {
  const game = new Trax('trax', '@0+')
  const analysis = analyze(game)
  const pre =
    'Analysis {\n' +
    '  game: trax(@0+),\n' +
    '  edge: tbd,\n' +
    '  threats: tbd,\n' +
    '  scores: tbd,\n' +
    '  score: tbd\n' +
    '}'
  t.is(inspect(analysis), pre, 'inspect before')
  t.is(analysis.score, 0, 'populate the data')
  const post =
    'Analysis {\n' +
    '  game: trax(@0+),\n' +
    '  edge: {\n' +
    '    w: arbrarbr,\n' +
    '    b: brarbrar,\n' +
    '    edge: [rawEdge]\n' +
    '  },\n' +
    '  threats: { b: {}, w: {} },\n' +
    '  scores: { w: 0, b: 0 },\n' +
    '  score: 0\n' +
    '}'
  t.is(inspect(analysis), post, 'inspect after')
})

test('suggestion inspection', (t) => {
  const inspection = 'A0/ 2, A0+ 0, A2+ 0, A2\\ -1, A2/ -2'
  const game = new Trax('trax', '@0/')
  const suggestion = suggest(game)
  t.is(inspect(suggestion).split(/\[|]/)[1], inspection, 'inspect string')
  t.is(suggestion.best?.move, 'A0/', 'best move')
})
