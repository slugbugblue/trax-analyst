# Trax Analyst

This project aims to provide tools for analyzing Trax games in Javascript.

## Description

Trax is a two-player boardless board game invented by David Smith. For more
information about Trax, including its [rules][traxrules] and
[history][traxhistory], see the official website at [traxgame.com][traxgame].

The goal of this javascript project is to allow Trax to be analyzed
programmatically. Eventually this will include the ability for a strong computer
opponent, however, this is still a work in progress.

The main driver for this effort is the [slugbugblue.com][sbb] website, with the
initial goal of getting a decent computer player for the [Trax
puzzles][trax-puzzles] available there.

### Analysis

The analysis engine is provided in `analyst.js`, and it can be used as follows:

```javascript
import { Trax } from '@slugbugblue/trax'
import { analyze } from '@slugbugblue/trax-analyst'

let trax = new Trax('trax', '@0/ @1\\')

let analysis = analyze(trax)

console.log('Edge analysis:', analysis.edge)
console.log('Threat analysis:', analysis.threats)
console.log('Score analysis:', analysis.scores)
```

For additional information, refer to the [analyst.js
documentation][docs-analyst].

### Threats database

The threats database is currently hand-coded in `threats.js`, so refer to that
file if you want to try to update the analysis engine's understanding of
threats.

## Roadmap

- Improve the threats database / analysis engine.
  - Bot should choose the strongest defense for each puzzle.
  - Bot should be able to win all puzzles.
  - Analysis should recognize line threats.

## Support

This project is built by [Chad Transtrum][ctrans] for [slugbugblue.com][sbb].
Issues can be opened on the [gitlab project page][repo].

## Contributing

[Contributions are welcome][contributing]. Ideally in the form of pull requests,
but feel free to open an issue with a bug report or a suggestion as well.

## License

Copyright 2019-2023 Chad Transtrum

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
the files in this project except in compliance with the License. You may obtain
a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

[contributing]: CONTRIBUTING.md
[ctrans]: mailto:chad@transtrum.net
[docs-analyst]: docs/analyst.md
[repo]: https://gitlab.com/slugbugblue/trax-analyst
[sbb]: https://slugbugblue.com/
[trax-puzzles]: https://gitlab.com/slugbugblue/trax-puzzles
[traxgame]: http://traxgame.com/
[traxhistory]: http://www.traxgame.com/about_history.php
[traxrules]: http://www.traxgame.com/about_rules.php
