# @slugbugblue/trax-analyst

## 0.12.0 - 2023-03-25

- Update to latest version of the trax engine

## 0.11.0 - 2023-03-24

- Additional threats for puzzles
- Pull analyst code from `@slugbugblue/trax` into its own package

## 0.10.0 - 2023-03-12

- Update the analysis engine to be more lazy

  - The LRU cache is more likely to have a hit because it no longer separates
    validated from unvalidated analyses
  - Suggestions are much faster because it now skips validations for low-scoring
    moves
  - You will now need to call `validateThreats()` to remove faulty threats
    manually

## 0.9.0 - 2023-02-11

- Add license information for puzzle sources
- Add gitlab CI configuration
- Drop support for node 14; node v16.15 is our minimum supported version
- Refine faulty threat detection and add test for one case it was failing

## 0.8.0 - 2023-02-06

- Added a simple LRU cache to increase the efficiency of the analysis engine

## 0.7.0 - 2023-02-05

- Analysis improvements:

  - Add `count.b(level)` and `count.w(level)` methods to an analysis to quickly
    count the number of threats at a given level
  - Remove faulty threats from an analysis
  - Add additional threats for more puzzle solving mojo

- Development environment enhancements:

  - Added an initial benchmarking framework with puzzle benchmarks so far
  - Added husky pre-commit hook to ensure npm test is run before a commit

## 0.6.0 - 2022-12-27

- Minor analysis improvements

## 0.5.0 - 2022-12-14

- Analysis suggestions now "solve" all tough puzzles
- Reworked threats to be more precise
- Reworked scoring to add more nuance

## 0.4.0 - 2022-12-05

- Analysis tests now include correctly suggesting the first move for solving all
  tutorial and easy puzzles. As the threats database improves, we will add more
  puzzles to the tests.

## 0.3.0 - 2022-11-25

- Add move suggestions to `analyst.js` for CLI and bot use

  - use `suggest(trax)` to get a suggestion for the current position
  - added the `suggest` CLI command for quick access to a random suggestion

- Begin work on documenting the javascript files using jsdoc, with the initial
  focus on types so typescript-powered autocomplete can be more useful

## 0.2.0 - 2022-10-28

- Fleshing out testing and debugging of `analyst.js`

## 0.1.0 - 2022-09-22

- Initial work on `analyst.js`, with documentation and a start on tests

  - use `analyze(trax)` to perform an analysis
  - added the `analyze` CLI command to see an analysis
