# Contributing to slugbugblue/trax-analyst

Hi, thanks for considering contributing to this project.

If this is your first time working on an open source project, it is recommended
that you start by reading [How to Contribute to Open Source][howto].

## Ways to contribute

### Improve documentation

One of the simplest ways to get started contributing is to find areas of the
documentation that are unclear, incorrect, or that have typos or other errors,
and providing feedback and corrections. You don't have to know how to code or
even how to use git.

### Identify bugs

If you are aware of something not working as you think it should, feel free to
open a new issue to let us know. Browsing through the existing open issues and
adding comments or making clarifications can also be a great way to get started.

### Fixing bugs

If you feel you have a solution to an existing bug, you are welcome to code up
your solution and submit a merge request. It may take some time to get fully up
to speed with our coding styles and with the structure of the code, so don't
feel bad if your merge request receives comments rather than being immediately
accepted. Simply make any necessary changes, and update the merge request, or
let us know in a subsequent comment if you think we are misunderstanding the
merge request.

## Submitting an issue

If you choose to submit an issue, please take the time to ensure that you
provide as much detail as possible about the issue. A poorly worded or poorly
researched issue report will unnecessarily delay the issue resolution.

Even if you don't have the time or expertise to fully diagnose the problem, a
good issue report can be very helpful to us as we seek to improve the code.

## Submitting a merge request

Good merge requests are a fantastic way to contribute to this project. Your
merge request is going to be accepted more quickly if it is easy to understand.
Ideally it should be focused on a single item and avoid unrelated commits.

Please ask first before embarking on a major change, such as adding a new
feature or refactoring the code, otherwise you risk spending a significant
amount of time working on something that will not be merged into the project.

If you have never submitted a merge request before, no fear. Here is a [quick
tutorial][howto-mr] on how to create one.

### Merge request steps

- Fork the project and clone it to your own workspace.
- Get the latest changes and update the project dependencies:

```bash
$ git checkout develop
$ git pull upstream develop
$ rm -rf node_modules
$ npm install
```

- Create a new branch to contain your changes:

```bash
$ git checkout -b <new-branch-name>
```

- Make your changes.
- Test your changes.

```bash
$ npm test
```

- Push your changes back to your fork.

```bash
$ git push -u origin <new-branch-name>
```

- Open a merge request in the [slugbugblue/trax-analyst][trax-analyst-repo]
  repo.

## Working with the code

This repository uses [XO][xo] for linting and [Prettier][prettier] for
formatting. Running `npm test` will produce an error if your changes do not
conform. You should consider installing the [XO plugin][xo-plugin] for your
editor to have the linting errors highlighted for you and automatically fixed on
save. You can also run `npx xo --fix` from the command line to fix most linting
errors.

This repository uses [AVA][ava] for writing and running tests. While we want as
much code coverage as possible for all code, we have a minimum requirement of
90% of code coverage for the [analysis engine][analyst]. You should be able to
get a quick grasp on the testing process by simply examining a few of the test
files in the [test directory][test].

[analyst]: src/analyst.js
[ava]: https://github.com/avajs/ava
[howto]: https://opensource.guide/how-to-contribute/
[howto-mr]: https://opensource.guide/how-to-contribute/#opening-a-pull-request
[prettier]: https://prettier.io
[test]: test/
[trax-analyst-repo]: https://gitlab.com/slugbugblue/trax-analyst
[xo]: https://github.com/sidresorhus/xo
[xo-plugin]: https://github.com/sidresorhus/xo#editor-plugins
